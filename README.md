# Scrut

Scrut is a tool designed to help maintaining Funtoo packages. It enables to track upstream new
versions as well as events on the Gentoo side that may allow Funtoo to unfork a given package.

## Usage

Here a simple example where notification is printed to stdout if the tracked package needs updates:

    $ python -m scrut examples/app-editors/sublime-text.scrut
    app-editors/sublime-text 3083 can be upgraded to 3126

See our [examples scripts][1] to check what this sublime-text.scrut file looks like.


[1]: https://gitlab.com/apinsard/scrut/tree/master/examples
