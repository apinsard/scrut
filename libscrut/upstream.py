# -*- coding: utf-8 -*-
import os
import re
import requests

from appi import Version

from ._base import BaseTracker


class UpstreamTracker(BaseTracker):

    def get_version_from_upstream_version(self, upstream_version):
        return Version(upstream_version)


class RegexVersionUpstreamTracker(UpstreamTracker):

    upstream_match_version_against = 'version'
    upstream_match_version_regex = r'^\s*(Version\s+)?v?(?P<V>[0-9][0-9a-z._-]*)\s*$'

    def get_upstream_match_version_regex(self):
        regex = self.upstream_match_version_regex
        if isinstance(regex, str):
            regex = re.compile(regex, re.I)
        elif isinstance(regex, tuple):
            regex = re.compile(*regex)
        return regex


class AtomUpstreamTracker(RegexVersionUpstreamTracker):

    upstream_atom_url = None
    upstream_match_version_against = 'id'
    upstream_match_version_regex = r'^.*/v?(?P<V>[0-9][0-9a-z._-]*)\s*$'

    def get_latest_version(self):
        import feedparser  # import here to make feedparser dependency optional
        feed = feedparser.parse(self.get_upstream_atom_url())
        regex = self.get_upstream_match_version_regex()
        for entry in feed.entries:
            version = getattr(entry, self.upstream_match_version_against)
            match = regex.match(version)
            if match:
                return self.get_version_from_upstream_version(match.group('V'))
        return None

    def get_upstream_atom_url(self):
        if not self.upstream_atom_url:
            class_name = self.__class__.__name__
            raise ValueError('{}.upstream_atom_url cannot be empty'.format(class_name))
        return self.upstream_atom_url


class GithubApiUpstreamTracker(RegexVersionUpstreamTracker):

    upstream_repository = None
    upstream_match_version_against = 'tag_name'
    upstream_api_url = 'https://api.github.com/repos/{repo}/releases/latest'

    def get_latest_version(self):
        r = requests.get(self.get_upstream_api_url())
        res = r.json()
        version = res[self.upstream_match_version_against]
        regex = self.get_upstream_match_version_regex()
        match = regex.match(version)
        if match:
            return self.get_version_from_upstream_version(match.group('V'))
        return None

    def get_upstream_api_url(self):
        url = self.upstream_api_url.format(repo=self.upstream_repository)
        client_id = os.environ.get('SCRUT_GITHUB_API_CLIENT_ID')
        client_secret = os.environ.get('SCRUT_GITHUB_API_CLIENT_SECRET')
        if client_id and client_secret:
            url += '?client_id={}&client_secret={}'.format(client_id, client_secret)
        return url


class GithubAtomUpstreamTracker(AtomUpstreamTracker):

    upstream_repository = None

    def get_upstream_atom_url(self):
        if not self.upstream_repository:
            class_name = self.__class__.__name__
            raise ValueError('{}.upstream_repository cannot be empty'.format(class_name))
        return 'https://github.com/{}/releases.atom'.format(self.upstream_repository)


class GitlabApiUpstreamTracker(RegexVersionUpstreamTracker):

    upstream_repository = None
    upstream_match_version_against = 'name'
    upstream_api_url = 'https://gitlab.com/api/v3/projects/{repo}/repository/tags'

    def get_latest_version(self):
        r = requests.get(self.get_upstream_api_url())
        tags = r.json()
        for tag in tags:
            version = tag[self.upstream_match_version_against]
            regex = self.get_upstream_match_version_regex()
            match = regex.match(version)
            if match:
                return self.get_version_from_upstream_version(match.group('V'))
        return None

    def get_upstream_api_url(self):
        return self.upstream_api_url.format(repo=self.upstream_repository.replace('/', '%2F'))
