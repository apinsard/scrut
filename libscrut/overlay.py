# -*- coding: utf-8 -*-
import os
from pathlib import Path
import subprocess

from appi import Ebuild

from ._base import BaseTracker


class OverlayTracker(BaseTracker):
    pass


class FilesystemOverlayTracker(OverlayTracker):

    overlay_path = None

    def get_current_version(self):
        versions = []
        for path in self.get_overlay_package_path().glob('*.ebuild'):
            ebuild = Ebuild(path)
            version = ebuild.get_version().get_upstream_version()
            if str(version).startswith('9999') or str(version).endswith('9999'):
                continue
            versions.append(version)
        if versions:
            return max(versions)
        else:
            return None

    def get_overlay_package_path(self):
        return self.get_overlay_path() / self.package

    def get_overlay_path(self):
        if not self.overlay_path:
            class_name = self.__class__.__name__
            raise ValueError('{}.overlay_path cannot be empty'.format(class_name))
        return Path(self.overlay_path)


class GitOverlayTracker(FilesystemOverlayTracker):

    overlay_url = None

    def get_overlay_url(self):
        return self.overlay_url

    def get_current_version(self):
        self.sync_overlay()
        return super().get_current_version()

    def sync_overlay(self):
        subprocess.Popen(
            'mkdir -p {dir} && cd {dir} && git clone {repo} . || git pull'.format(
                dir=self.get_overlay_path(), repo=self.get_overlay_url()),
            shell=True, stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)

    def get_overlays_root(self):
        return Path(os.environ.get('SCRUT_OVERLAYS_ROOT', '/var/tmp/scrut_overlays'))

    def get_overlay_path(self):
        return self.get_overlays_root() / self.get_overlay_url().split('/')[-1]

    def get_repository_link(self):
        return self.get_overlay_url()


class FuntooGitOverlayTracker(GitOverlayTracker):

    repository = None

    def get_overlay_url(self):
        return 'http://git.funtoo.org/{}'.format(self.repository)

    def get_overlay_path(self):
        return self.get_overlays_root() / self.repository

    def get_package_link(self):
        return '{}/tree/{}'.format(self.get_overlay_url(), self.package)


class GithubGitOverlayTracker(GitOverlayTracker):

    repository = None

    def get_overlay_url(self):
        return 'http://github.com/{}'.format(self.repository)

    def get_overlay_path(self):
        return self.get_overlays_root() / self.repository

    def get_package_link(self):
        return '{}/tree/master/{}'.format(self.get_overlay_url(), self.package)
