# -*- coding: utf-8 -*-
import requests
from xml.etree import ElementTree

from ._base import BaseTracker


class UnforkTracker(BaseTracker):

    def should_be_unforked(self):
        return False


class GentooBugUnforkTracker(UnforkTracker):
    """Unfork when a Gentoo bug matches the given requirement."""

    unfork_when = ('bug_status', 'RESOLVED')
    unfork_bug_id = None

    def should_be_unforked(self):
        prop, expected = self.get_unfork_when()
        node = self.get_unfork_bug_node()
        values = list(node.find(prop).itertext())
        return expected in values

    def get_unfork_bug_node(self):
        r = requests.get(self.get_unfork_bug_url())
        return ElementTree.fromstring(r.text).find('bug')

    def get_unfork_bug_url(self):
        return 'https://bugs.gentoo.org/show_bug.cgi?ctype=xml&id={}'.format(self.unfork_bug_id)

    def get_unfork_when(self):
        return self.unfork_when

    def get_unfork_bug_id(self):
        return self.unfork_bug_id

    def get_unfork_indication(self):
        return 'BGO #{} ({} -> {})'.format(self.get_unfork_bug_id(), *self.get_unfork_when())

    def get_unfork_indication_link(self):
        return 'https://bugs.gentoo.org/show_bug.cgi?id={}'.format(self.get_unfork_bug_id())


class GentooTreeUnforkTracker(UnforkTracker):
    """Unfork when package is added to Gentoo tree."""

    gentoo_possible_other_names = []

    def should_be_unforked(self):
        url = 'https://gitweb.gentoo.org/repo/gentoo.git/tree/{}'
        for package in [self.package] + self.gentoo_possible_other_names:
            if requests.get(url.format(package)).status_code == 200:
                return True
        return False

    def get_unfork_indication(self):
        return "When present in gentoo tree"

    def get_unfork_indication_link(self):
        return 'https://gitweb.gentoo.org/repo/gentoo.git/tree/'
