# -*- coding: utf-8 -*-


class BaseTracker:

    package = None
    repository = None
    bug_id = None
    unfork_indication = None

    def get_package(self):
        return self.package

    def get_package_link(self):
        return None

    def get_repository(self):
        return self.repository

    def get_repository_link(self):
        return None

    def get_current_version(self):
        return None

    def get_current_version_link(self):
        return None

    def get_latest_version(self):
        return None

    def get_latest_version_link(self):
        return None

    def get_related_bug(self):
        return self.bug_id

    def get_related_bug_link(self):
        if self.bug_id:
            return 'https://bugs.funtoo.org/browse/{}'.format(self.bug_id)
        else:
            return None

    def get_unfork_indication(self):
        return self.unfork_indication

    def get_unfork_indication_link(self):
        return None
