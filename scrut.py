# -*- coding: utf-8 -*-
# flake8: noqa
import json
import sys

from libscrut import *

DEBUG = False

html_template = """
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet"
        href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
        integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ"
        crossorigin="anonymous">
</head>
<body>
    <h1>Funtoo Forked Packages</h1>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Status</th>
                <th>Package</th>
                <th>Repository</th>
                <th>Current Version</th>
                <th>Latest Version</th>
                <th>Related Bug</th>
                <th>Unfork Indication</th>
            </tr>
        </thead>
        <tbody>
            {}
        </tbody>
    </table>
</body>
</html>
"""


def get_value_of(tracker, attribute, *args, **kwargs):
    value = getattr(tracker, attribute, None)
    if callable(value):
        try:
            value = value(*args, **kwargs)
        except NotImplementedError:
            value = None
    return value

def get_link(tracker, attribute):
    text = getattr(tracker, 'get_{}'.format(attribute))() or '&ndash;'
    link = getattr(tracker, 'get_{}_link'.format(attribute))()
    if link:
        return '<a href="{}">{}</a>'.format(link, text)
    else:
        return text


if __name__ == '__main__':
    results = []
    html_rows = []
    for filename in sys.argv[1:]:
        with open(filename, 'rb') as f:
            exec(compile(f.read(), filename, 'exec'), globals(), locals())
        try:
            tracker = Tracker()
        except NameError as e:
            sys.stderr.write('{} does not define a Tracker class.\n'.format(filename))
            if DEBUG:
                raise e
            continue

        package = tracker.package

        try:
            current_version = get_value_of(tracker, 'get_current_version')
        except Exception as e:
            sys.stderr.write('Failed to fetch current version for {}\n'.format(package))
            if DEBUG:
                raise e
            continue
        try:
            latest_version = get_value_of(tracker, 'get_latest_version')
        except Exception as e:
            sys.stderr.write('Failed to fetch latest version for {}\n'.format(package))
            if DEBUG:
                raise e
            continue
        try:
            unfork = get_value_of(tracker, 'should_be_unforked')
        except Exception as e:
            sys.stderr.write('Failed to check if {} can be unforked\n'.format(package))
            if DEBUG:
                raise e
            continue

        result = {
            'package': package,
            'current_version': str(current_version) if current_version else None,
            'latest_version': str(latest_version) if latest_version else None,
            'unfork': unfork,
        }
        if current_version and latest_version:
            result['bump'] = current_version != latest_version
        else:
            result['bump'] = None
        if hasattr(tracker, 'bug_id'):
            result['bug_id'] = tracker.bug_id
        if hasattr(tracker, 'unfork_bug_id'):
            result['unfork_bug_id'] = tracker.unfork_bug_id

        state = 'Up to date'
        state_class = 'table-success'
        if result['bump']:
            state = 'Bump required'
            state_class = 'table-warning'
            print('{} {} can be upgraded to {}'.format(package, current_version, latest_version))
        if result['unfork']:
            state = 'Unfork required'
            state_class = 'table-danger'
            print('{} can be unforked'.format(package))
        results.append(result)

        html_cells = [
            '<td class="{}">{}</td>'.format(state_class, state),
        ] + [
            '<td>{}</td>'.format(get_link(tracker, attr)) for attr in [
                'package', 'repository', 'current_version', 'latest_version',
                'related_bug', 'unfork_indication'
            ]
        ]
        html_rows.append('<tr>{}</tr>'.format('\n'.join(html_cells)))

    with open('scrut.json', 'w') as f:
        f.write(json.dumps(results, indent=2))

    with open('scrut.html', 'w') as f:
        f.write(html_template.format('\n'.join(html_rows)))
